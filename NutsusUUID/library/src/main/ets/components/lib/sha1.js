import { CryptoJS } from '@ohos/crypto-js';
import buffer from '@ohos.buffer';

function sha1(bytes) {
  if (Array.isArray(bytes)) {
    bytes = buffer.from(bytes);
  } else if (typeof bytes === 'string') {
    bytes = buffer.from(bytes, 'utf8');
  }

  return CryptoJS.createHash('sha1').update(bytes).digest();
}

export default sha1;
