

感谢大家一起参与坚果派三方库的移植中来。

# 目前检测过的库

## 有兼容性问题的库

https://www.npmjs.com/package/markdown-it

https://www.npmjs.com/package/uuid

## 无问题的库

https://github.com/felixge/node-dateformat

https://github.com/sindresorhus/string-length

https://github.com/mathiasbynens/emoji-regex



## 目前移植NPM仓的三条路

1 无依赖的JS库直接迁移即可

2 有依赖的JS库需要侵入式修改

3 直接纯手工迁移

## FAQ

像无依赖的开源库里面的代码，一般是将哪些层级的内容进行适配，像你ppt示例中是讲src里面的所有文件以及一个根目录下的index进行了移植，大部分的开源库移植都这样吗？

答：是的，src为库主代码，index是对外提供接口的文件，有些库已经适配了ts，这种情况会存在.d.ts文件，直接拿来用就可以，如果没有ts文件，得自己写一份对外暴露接口的.d.ts文件

https://gitee.com/openharmony-tpc/pinyin4js/tree/master

github.com/superbiger/pinyin4js

可以看看这种已经适配好的库